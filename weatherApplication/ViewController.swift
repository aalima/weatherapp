//
//  ViewController.swift
//  weatherApplication
//
//  Created by Alima Aglakova on 19/02/2021.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var feelsLikeTemp: UILabel!
    @IBOutlet weak var changeCityButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var url = ""
    var myData: Model?
    
    private var decoder: JSONDecoder = JSONDecoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(CollectionViewCell.nib, forCellWithReuseIdentifier: CollectionViewCell.identifier)
        changeCoordinates(cityCoord: Constants.cities["Astana"] ?? "", cityNamee: "Astana" )
        fetchData()
    }
    
    private func changeCoordinates(cityCoord : String, cityNamee : String){
        self.cityName.text = cityNamee
        self.url = "\(Constants.host + cityCoord)&exclude=minutely,alerts&appid=\(Constants.apiKey)&units=metric"
    }

    @IBAction func didTapChangeCityButton(_ sender: Any) {
        let alert = UIAlertController(title: "Choose city", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        alert.addAction(UIAlertAction(title: "Almaty", style: .default, handler: {_ in
            self.changeCoordinates(cityCoord:  Constants.cities["Almaty"] ?? "", cityNamee: "Almaty")
            self.fetchData()
        }))
        
        alert.addAction(UIAlertAction(title: "Aktau", style: .default, handler: {_ in
            self.changeCoordinates(cityCoord:  Constants.cities["Aktau"] ?? "", cityNamee: "Aktau")
            self.fetchData()
        }))
        
        alert.addAction(UIAlertAction(title: "Atyrau", style: .default, handler: {_ in
            self.changeCoordinates(cityCoord:  Constants.cities["Atyrau"] ?? "", cityNamee: "Atyrau")
            self.fetchData()
        }))
        
        alert.addAction(UIAlertAction(title: "Astana", style: .default, handler: {_ in
            self.changeCoordinates(cityCoord:  Constants.cities["Astana"] ?? "", cityNamee: "Astana")
            self.fetchData()
        }))
  
        self.present(alert, animated: true)
        
    }
    
    func updateUI(){
        cityName.text = myData?.timezone
        temp.text = "\(myData?.current?.temp ?? 0.0) °C"
        feelsLikeTemp.text = "\(myData?.current?.feels_like ?? 0.0) °C"
        desc.text = myData?.current?.weather?.first?.description
        self.collectionView.reloadData()
        self.tableView.reloadData()
    }
    
    func fetchData(){
        AF.request(url).responseJSON { (response) in
            switch response.result{
            case .success(_):
                guard let data = response.data else { return }
                guard let answer = try? self.decoder.decode(Model.self, from: data) else{ return }
                self.myData = answer
                self.updateUI()
            case .failure(let err):
                print(err.errorDescription ?? "")
            }
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myData?.daily?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        let item = self.myData?.daily?[indexPath.row]
        cell.day.text = dayFor(index: indexPath.row)
        cell.desc.text = item?.weather?.first?.description
        cell.dayTemp.text = "Day : " + String(item?.temp?.day ?? 0) + "°C"
        cell.nightTemp.text = "Night : " + String(item?.temp?.night ?? 0) + "°C"
        cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 1
            cell.isSelected = true
        cell.layer.cornerRadius = 6
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(100)
    }
    
    private func dayFor(index : Int) -> String{
        guard let day_date = myData?.daily?[index].dt else { return "" }
        let date = Date(timeIntervalSince1970: day_date)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "EEEE, MMM d"
        let localDate = dateFormatter.string(from: date)
        return String(localDate)
    }
}

extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myData?.hourly?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as! CollectionViewCell
        let item = myData?.hourly?[indexPath.item]
        cell.temp.text = "\(item?.temp ?? 0.0)"
        cell.feelsLike.text = "\(item?.feels_like ?? 0.0)"
        cell.desc.text = item?.weather?.first?.description
        return cell
    }
}

extension ViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}
