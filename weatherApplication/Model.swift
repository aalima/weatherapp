//
//  Model.swift
//  weatherApplication
//
//  Created by Alima Aglakova on 19/02/2021.
//

import Foundation

public struct Model: Codable{
    let timezone: String?
    let hourly: [Current]?
    let daily: [Daily]?
    let current: Current?
    let name: String?
}

struct Current: Codable {
    let temp: Double?
    let feels_like: Double?
    let weather: [Weather]?
}

struct Hourly : Codable{
    let dt : Double
    let temp: Double?
    let feels_like: Double?
}

struct Daily: Codable {
    let dt : Double
    let temp: Temp?
    let feels_like: Temp?
    let weather: [Weather]?
}

struct Temp: Codable {
    let day: Double?
    let night: Double?
}

struct Weather: Codable {
    let main: String?
    let description: String?
}
