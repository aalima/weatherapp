//
//  Constants.swift
//  weatherApplication
//
//  Created by Alima Aglakova on 19/02/2021.
//

import Foundation

struct Constants {
    static let host = "https://api.openweathermap.org/data/2.5/onecall?lat=51.1801&lon=71.446&exclude=minutely,alerts&appid=0e85e7c4a26e9fd2925507ebe21af26&units=metric"
    static let cities: [String: String] = ["Astana":"lat=51.1801&lon=71.446", "Almaty":"lat=43.238949&lon=76.889709", "Aktau":"lat=43.693695&lon=51.260834", "Atyrau":"lat=47.105045&lon=51.924622"]
    static let apiKey = "0e85e7c4a26e9fd2925507ebe21af26c"
}
